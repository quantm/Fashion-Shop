SET IDENTITY_INSERT [dbo].[Brands] ON
INSERT INTO [dbo].[Brands] ([BrandID], [Name], [ImageURL], [Description], [URL], [UserID], [Modified], [Created]) VALUES (1, N'Gucci', NULL, NULL, N'gucci', 1, N'2013-12-15 13:58:15', N'2013-12-15 13:58:15')
INSERT INTO [dbo].[Brands] ([BrandID], [Name], [ImageURL], [Description], [URL], [UserID], [Modified], [Created]) VALUES (2, N'Versace', NULL, NULL, N'versace', 1, N'2013-12-15 13:58:37', N'2013-12-15 13:58:37')
INSERT INTO [dbo].[Brands] ([BrandID], [Name], [ImageURL], [Description], [URL], [UserID], [Modified], [Created]) VALUES (3, N'Louis Vuitton', NULL, NULL, N'louis-vuitton', 1, N'2013-12-15 13:59:52', N'2013-12-15 13:59:52')
INSERT INTO [dbo].[Brands] ([BrandID], [Name], [ImageURL], [Description], [URL], [UserID], [Modified], [Created]) VALUES (4, N'Prada', NULL, NULL, N'prada', 1, N'2013-12-15 14:00:01', N'2013-12-15 14:00:01')
INSERT INTO [dbo].[Brands] ([BrandID], [Name], [ImageURL], [Description], [URL], [UserID], [Modified], [Created]) VALUES (5, N'Armani', NULL, NULL, N'armani', 1, N'2013-12-15 14:21:17', N'2013-12-15 14:00:52')
INSERT INTO [dbo].[Brands] ([BrandID], [Name], [ImageURL], [Description], [URL], [UserID], [Modified], [Created]) VALUES (6, N'Dolce & Gabbana', NULL, NULL, N'dolce-gabbana', 1, N'2013-12-15 14:04:35', N'2013-12-15 14:04:35')
SET IDENTITY_INSERT [dbo].[Brands] OFF
