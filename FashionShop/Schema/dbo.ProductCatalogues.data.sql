SET IDENTITY_INSERT [dbo].[ProductCatalogues] ON
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (1, N'Quần', NULL, N'quan', 1, N'2013-12-15 14:28:07', N'2013-12-15 14:28:07')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (2, N'Áo', NULL, N'ao', 1, N'2013-12-15 14:28:11', N'2013-12-15 14:28:11')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (3, N'Váy', NULL, N'vay', 1, N'2013-12-15 14:28:14', N'2013-12-15 14:28:14')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (4, N'Mũ', NULL, N'mu', 1, N'2013-12-15 14:28:19', N'2013-12-15 14:28:19')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (5, N'Kính', NULL, N'kinh', 1, N'2013-12-15 14:28:24', N'2013-12-15 14:28:24')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (6, N'Đồng Hồ', NULL, N'dong-ho', 1, N'2013-12-15 14:28:29', N'2013-12-15 14:28:29')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (7, N'Áo lót', NULL, N'ao-lot', 1, N'2013-12-15 14:28:33', N'2013-12-15 14:28:33')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (8, N'Quần lót', NULL, N'quan-lot', 1, N'2013-12-15 14:28:37', N'2013-12-15 14:28:37')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (9, N'Túi xách', NULL, N'tui-xach', 1, N'2013-12-15 14:31:11', N'2013-12-15 14:31:11')
INSERT INTO [dbo].[ProductCatalogues] ([ProductCatalogueID], [Name], [Description], [URL], [UserID], [Modified], [Created]) VALUES (10, N'Giày, guốc', NULL, N'giay,-guoc', 1, N'2013-12-15 14:40:18', N'2013-12-15 14:40:18')
SET IDENTITY_INSERT [dbo].[ProductCatalogues] OFF
