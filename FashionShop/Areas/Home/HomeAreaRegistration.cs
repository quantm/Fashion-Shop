﻿using System.Web.Mvc;

namespace FashionShop.Areas.Home
{
    public class HomeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Home";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // routes.MapRoute(
            //    name: "Hello",
            //    url: "Hello/{id}",
            //    defaults: new { controller = "Hello", action = "World", id = UrlParameter.Optional }
            //);
            context.MapRoute(
                "San pham",
                "san-pham/{id}/{url}",
                new { controller = "Product", action = "Index", id = UrlParameter.Optional }
                //new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Home_default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                //new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
