﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;
namespace FashionShop.Areas.Home.Controllers
{
    public class HomeController : HomeAppController
    {
        //
        // GET: /Home/Home/
        

        public ActionResult Index()
        {
            //New Products
            var product = db.Product.Include(p => p.Brand).Include(p => p.ProductCatalogue);
            ViewBag.Products = product.OrderByDescending(m => m.Created).Take(16).ToList();
            //Quan ao
            var underwear = db.Product.Include(p => p.Brand).Include(p => p.ProductCatalogue);
            ViewBag.UnderwearProducts = underwear.OrderByDescending(m => m.Created)
                .Where(m => m.ProductCatalogueID == 8)
                .ToList(); 

            //QUan
            var pants = db.Product.Include(p => p.Brand).Include(p => p.ProductCatalogue);
            ViewBag.PantProducts = pants.OrderByDescending(m => m.Created)
                .Where(m => m.ProductCatalogueID == 1)
                .ToList(); 
            //Ao
            var coat =
                db.Product.Include(p => p.Brand).Include(p => p.ProductCatalogue);
            ViewBag.CoatProducts = coat.OrderByDescending(m => m.Created)
                .Where(m => m.ProductCatalogueID == 2)
                .Take(5)
                .ToList(); 

            return View();
        }

    }
}
