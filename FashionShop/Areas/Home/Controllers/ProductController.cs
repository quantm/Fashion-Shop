﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;
namespace FashionShop.Areas.Home.Controllers
{
    public class ProductController : HomeAppController
    {
        //
        // GET: /Home/Product/

        public ActionResult Index(int id, string url)
        {
            //Response.Write(url);
            
            Product product  = db.Product.Find(id);
            //Same catalogue
            var same_catalogue_product =
                db.Product.Where(m => m.ProductCatalogueID == product.ProductCatalogueID && m.ProductID != product.ProductID)
                .Take(5).ToList();

            //Same brand
            var same_brand_product =
                db.Product.Where(m => m.BrandID == product.BrandID && m.ProductID != product.ProductID && m.ProductCatalogueID != product.ProductCatalogueID)
                .Take(5).ToList();


            ViewBag.product = product;
            ViewBag.SameCatalogue = same_catalogue_product;
            ViewBag.SameBrand = same_brand_product;
            return View();
        }

    }
}
