﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;

namespace FashionShop.Areas.Admin.Controllers
{
    public class OptionController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/Option/

        public ActionResult Index()
        {
            return View(db.Options.ToList());
        }

        //
        // GET: /Admin/Option/Details/5

        public ActionResult Details(int id = 0)
        {
            Option option = db.Options.Find(id);
            if (option == null)
            {
                return HttpNotFound();
            }
            return View(option);
        }

        //
        // GET: /Admin/Option/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Option/Create

        [HttpPost]
        public ActionResult Create(Option option)
        {
            if (ModelState.IsValid)
            {
                db.Options.Add(option);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(option);
        }

        //
        // GET: /Admin/Option/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Option option = db.Options.Find(id);
            if (option == null)
            {
                return HttpNotFound();
            }
            return View(option);
        }

        //
        // POST: /Admin/Option/Edit/5

        [HttpPost]
        public ActionResult Edit(Option option)
        {
            if (ModelState.IsValid)
            {
                db.Entry(option).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(option);
        }

        //
        // GET: /Admin/Option/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Option option = db.Options.Find(id);
            if (option == null)
            {
                return HttpNotFound();
            }
            return View(option);
        }

        //
        // POST: /Admin/Option/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Option option = db.Options.Find(id);
            db.Options.Remove(option);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}