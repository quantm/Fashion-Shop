﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using FashionShop.Models;
namespace FashionShop.Areas.Admin.Controllers
{
    public class AdminAppController : Controller
    {
        protected  ModelDBContext db = new ModelDBContext();
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
          
            var s = Session["Username"];
            if (s == null && Request.RawUrl.IndexOf("/Admin/User/Login") == -1)
            {
                Response.Redirect("/Admin/User/Login");
            }
            base.OnResultExecuting(filterContext);
        }

        public void die()
        {
            Response.End();
        }

        public void pr(Object o, bool die = false)
        {
            Response.Write(o.ToString());
            if (die)
                Response.End();
        }

    }   
}
