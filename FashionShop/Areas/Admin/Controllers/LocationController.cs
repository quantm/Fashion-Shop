﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;

namespace FashionShop.Areas.Admin.Controllers
{
    public class LocationController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/Location/

        public ActionResult Index()
        {
  
            return View(db.Location.ToList());
        }

        //
        // GET: /Admin/Location/Details/5

        public ActionResult Details(int id = 0)
        {
            Location location = db.Location.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        //
        // GET: /Admin/Location/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Location/Create

        [HttpPost]
        public ActionResult Create(Location location)
        {
            if (ModelState.IsValid)
            {
                Location l = db.Location.Where(m => m.Name == location.Name).FirstOrDefault();
                
                if (l != null)
                {
                    ModelState.AddModelError("Name", "Tên quốc gia đã tồn tại");
                    return View(location);
                }
                
                location.AddCreatedTime();
                db.Location.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(location);
        }

        //
        // GET: /Admin/Location/Edit/5

        public ActionResult Edit(int id = 0)
        {

            Location location = db.Location.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        //
        // POST: /Admin/Location/Edit/5

        [HttpPost]
        public ActionResult Edit(Location location)
        {
            if (ModelState.IsValid)
            {

                location.UpdateModifiedTime();
                db.Entry(location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(location);
        }

        //
        // GET: /Admin/Location/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Location location = db.Location.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        //
        // POST: /Admin/Location/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Location location = db.Location.Find(id);
            db.Location.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}