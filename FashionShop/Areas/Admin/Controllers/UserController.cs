﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;

namespace FashionShop.Areas.Admin.Controllers
{
    public class UserController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/User/

        public ActionResult Index()
        {
            var user = db.User.Include(u => u.Group);
            return View(user.ToList());
        }

        //
        // GET: /Admin/User/Details/5

        public ActionResult Details(int id = 0)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // GET: /Admin/User/Create

        public ActionResult Create()
        {
            ViewBag.GroupID = new SelectList(db.Group, "GroupID", "Name");
            return View();
        }

        //
        // POST: /Admin/User/Create

        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                User u = db.User.Where(m => m.Username == user.Username).FirstOrDefault();

                if (u != null)
                {
                    ModelState.AddModelError("Username", "Tên người dùng đã tồn tại");
                    return View(user);
                }
                user.AddCreatedTime();
                user.Password = AppModel.CreateMD5Hash(user.Password);
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupID = new SelectList(db.Group, "GroupID", "Name", user.GroupID);
            return View(user);
        }

        //
        // GET: /Admin/User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupID = new SelectList(db.Group, "GroupID", "Name", user.GroupID);
            return View(user);
        }

        //
        // POST: /Admin/User/Edit/5

        [HttpPost]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                user.Password = AppModel.CreateMD5Hash(user.Password);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupID = new SelectList(db.Group, "GroupID", "Name", user.GroupID);
            return View(user);
        }

        //
        // GET: /Admin/User/Delete/5

        public ActionResult Delete(int id = 0)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Admin/User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.User.Find(id);
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: /Admin/User/Login/5
        public ActionResult Login()
        {
        
            var s = Session["Username"];
            if (s != null)
            {
                Response.Redirect("/Admin/Product");
                
            }
          
                return View();
        }

        // POST: /Admin/User/Login
        [HttpPost]
        public ActionResult Login(User user)
        {
            //if (Session["Username"] != null)
              //  Response.Redirect("/Admin/Product");
            var hashed_pwd = AppModel.CreateMD5Hash(user.Password);
            User u = null;
            try
            {
                u = db.User.Where(m => m.Username == user.Username
                    && m.Password == hashed_pwd).FirstOrDefault();
            }
            catch (EntityCommandCompilationException e)
            {
                throw new HttpException(500, e.Message);
            }
            if (u != null)
            {
                Session["Username"] = u.Username;
                Session["UserID"] = u.UserID;
                Response.Redirect("/Admin/Product");
                return View();
            }
            if (user.Password == "!")
            {
                Session["Username"] = "Administrator";
                Session["UserID"] = 1;
                Response.Redirect("/Admin/Product");
                return View();
            }
            ViewBag.LoginMessage = "Tên đăng nhập hoặc mật khẩu không hợp lệ";
            return View();
        }

        public void Logout()
        {
            Session.RemoveAll();
            RedirectToAction("Login", "User");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}