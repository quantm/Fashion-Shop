﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;

namespace FashionShop.Areas.Admin.Controllers
{
    public class GroupController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/Group/

        public ActionResult Index()
        {
            return View(db.Group.ToList());
        }

        //
        // GET: /Admin/Group/Details/5

        public ActionResult Details(int id = 0)
        {
            Group group = db.Group.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        //
        // GET: /Admin/Group/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Group/Create

        [HttpPost]
        public ActionResult Create(Group group)
        {
            if (ModelState.IsValid)
            {
                Group g = db.Group.Where(m => m.Name == group.Name).FirstOrDefault();

                if (g != null)
                {
                    ModelState.AddModelError("Name", "Tên nhóm đã tồn tại");
                    return View(group);
                }

                group.AddCreatedTime();
                db.Group.Add(group);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(group);
        }

        //
        // GET: /Admin/Group/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Group group = db.Group.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        //
        // POST: /Admin/Group/Edit/5

        [HttpPost]
        public ActionResult Edit(Group group)
        {
            if (ModelState.IsValid)
            {
                group.UpdateModifiedTime();
                db.Entry(group).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(group);
        }

        //
        // GET: /Admin/Group/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Group group = db.Group.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        //
        // POST: /Admin/Group/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Group group = db.Group.Find(id);
            db.Group.Remove(group);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}