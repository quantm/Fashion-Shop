﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;
using System.IO;

namespace FashionShop.Areas.Admin.Controllers
{
    public class BrandController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/Brand/

        public ActionResult Index()
        {
            
            int page = 1;
            //throw new HttpException(404, "FUCK");   
            if (!String.IsNullOrEmpty(Request.QueryString["page"]))
                page = Int32.Parse(Request.QueryString["page"]);

            int row_per_page = 5;

            //Response.End();
            ViewBag.CurrentPage = page;
            ViewBag.MaxPage = (int)Math.Ceiling((double)db.Brand.Count() / (double)row_per_page);

            if (page != 1 && page > ViewBag.MaxPage)
            {
                return HttpNotFound();
            }
            var brands = db.Brand.OrderByDescending(m => m.Created).Skip((page - 1) * row_per_page).Take(row_per_page).ToList();
            return View(brands);
        }

        //
        // GET: /Admin/Brand/Details/5

        public ActionResult Details(int id = 0)
        {
            Brand brand = db.Brand.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        //
        // GET: /Admin/Brand/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Brand/Create

        [HttpPost]
        public ActionResult Create(Brand brand)
        {
            if (ModelState.IsValid)
            {

                Brand b = db.Brand.Where(m => m.Name == brand.Name).FirstOrDefault();
                if (b != null)
                {
                    ModelState.AddModelError("Name", "Tên thương hiệu đã tồn tại");
                    return View(brand);
                }

                brand.AddCreatedTime();
                brand.URL = UrlUtil.GenerateUrl(brand.Name);
                brand.UserID = Int32.Parse(Session["UserID"].ToString());
                //Upload Image
                            
                db.Brand.Add(brand);
                db.SaveChanges();
                HttpPostedFileBase photo = Request.Files["Image"];   
                if (photo != null)
                {
                    photo.SaveAs(Path.Combine(Server.MapPath("~/Content/img/brand"), brand.BrandID + ".jpg"));
                }

                return RedirectToAction("Index");
            }

            return View(brand);
        }

        //
        // GET: /Admin/Brand/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Brand brand = db.Brand.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        //
        // POST: /Admin/Brand/Edit/5

        [HttpPost]
        public ActionResult Edit(Brand brand)
        {
            if (ModelState.IsValid)
            {
                brand.UpdateModifiedTime();
                brand.URL = UrlUtil.GenerateUrl(brand.Name);
                brand.UserID = Int32.Parse(Session["UserID"].ToString());
                db.Entry(brand).State = EntityState.Modified;
                db.SaveChanges();
                HttpPostedFileBase photo = Request.Files["Image"];
                if (photo.ContentLength > 0)
                {
                    photo.SaveAs(Path.Combine(Server.MapPath("~/Content/img/brand"), brand.BrandID + ".jpg"));
                }
                return RedirectToAction("Index");
            }
            return View(brand);
        }

        //
        // GET: /Admin/Brand/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Brand brand = db.Brand.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        //
        // POST: /Admin/Brand/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Brand brand = db.Brand.Find(id);
            db.Brand.Remove(brand);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}