﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;
using System.IO;
namespace FashionShop.Areas.Admin.Controllers
{
    public class ProductController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/Product/

        public ActionResult Index()
        {
            int page = 1;
            //throw new HttpException(404, "FUCK");   
            if(!String.IsNullOrEmpty(Request.QueryString["page"]))
                page = Int32.Parse(Request.QueryString["page"]);

            int row_per_page = 5;
          
            //Response.End();
            ViewBag.CurrentPage = page;            
            ViewBag.MaxPage = (int) Math.Ceiling((double)db.Product.Count() / (double)row_per_page);

            if (page != 1 && page > ViewBag.MaxPage)
            {
                return HttpNotFound();
            }


            var product = db.Product.Include(p => p.Brand).Include(p => p.ProductCatalogue);
            return View(product.OrderByDescending(m => m.Created).Skip((page - 1) * row_per_page).Take(row_per_page).ToList());
        }

        //
        // GET: /Admin/Product/Details/5

        public ActionResult Details(int id = 0)
        {
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // GET: /Admin/Product/Create

        public ActionResult Create()
        {
            ViewBag.LocationID = new SelectList(db.Location, "LocationID", "Name");
            ViewBag.BrandID = new SelectList(db.Brand, "BrandID", "Name");
            ViewBag.ProductCatalogueID = new SelectList(db.ProductCatalogue, "ProductCatalogueID", "Name");
            return View();
        }

        //
        // POST: /Admin/Product/Create

        [HttpPost]
        public ActionResult Create(Product product)
        {
            if (ModelState.IsValid)
            {
                product.AddCreatedTime();
                product.URL = UrlUtil.GenerateUrl(product.Name, "html");
                product.UserID = Int32.Parse(Session["UserID"].ToString());
                db.Product.Add(product);
                db.SaveChanges();
                HttpPostedFileBase photo = Request.Files["Image"];
                if (photo.ContentLength > 0)
                {
                    photo.SaveAs(Path.Combine(Server.MapPath("~/Content/img/product"), product.ProductID + ".jpg"));
                }
                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(db.Location, "LocationID", "Name", product.LocationID);
            ViewBag.BrandID = new SelectList(db.Brand, "BrandID", "Name", product.BrandID);
            ViewBag.ProductCatalogueID = new SelectList(db.ProductCatalogue, "ProductCatalogueID", "Name", product.ProductCatalogueID);
            return View(product);
        }

        //
        // GET: /Admin/Product/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(db.Location, "LocationID", "Name", product.LocationID);
            ViewBag.BrandID = new SelectList(db.Brand, "BrandID", "Name", product.BrandID);
            ViewBag.ProductCatalogueID = new SelectList(db.ProductCatalogue, "ProductCatalogueID", "Name", product.ProductCatalogueID);
            return View(product);
        }

        //
        // POST: /Admin/Product/Edit/5

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                product.UpdateModifiedTime();
                product.URL = UrlUtil.GenerateUrl(product.Name, "html");
                product.UserID = Int32.Parse(Session["UserID"].ToString());
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                HttpPostedFileBase photo = Request.Files["Image"];
                if (photo.ContentLength > 0)
                {
                    photo.SaveAs(Path.Combine(Server.MapPath("~/Content/img/product"), product.ProductID + ".jpg"));
                }
                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(db.Location, "LocationID", "Name", product.LocationID);
            ViewBag.BrandID = new SelectList(db.Brand, "BrandID", "Name", product.BrandID);
            ViewBag.ProductCatalogueID = new SelectList(db.ProductCatalogue, "ProductCatalogueID", "Name", product.ProductCatalogueID);
            return View(product);
        }

        //
        // GET: /Admin/Product/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Admin/Product/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Product.Find(id);
            db.Product.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}