﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;

namespace FashionShop.Areas.Admin.Controllers
{
    public class ProductCatalogueController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/ProductCatalogue/

        public ActionResult Index()
        {
            int page = 1;
            //throw new HttpException(404, "FUCK");   
            if (!String.IsNullOrEmpty(Request.QueryString["page"]))
                page = Int32.Parse(Request.QueryString["page"]);

            int row_per_page = 5;
            ViewBag.CurrentPage = page;
            ViewBag.MaxPage = (int)Math.Ceiling((double)db.ProductCatalogue.Count() / (double)row_per_page);

            if (page != 1 && page > ViewBag.MaxPage)
            {
                return HttpNotFound();
            }


            var catalogues = db.ProductCatalogue.OrderByDescending(m => m.Created).Skip((page - 1) * row_per_page).Take(row_per_page).ToList();
            return View(catalogues);
        }

        //
        // GET: /Admin/ProductCatalogue/Details/5

        public ActionResult Details(int id = 0)
        {
            ProductCatalogue productcatalogue = db.ProductCatalogue.Find(id);
            if (productcatalogue == null)
            {
                return HttpNotFound();
            }
            return View(productcatalogue);
        }

        //
        // GET: /Admin/ProductCatalogue/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/ProductCatalogue/Create

        [HttpPost]
        public ActionResult Create(ProductCatalogue productcatalogue)
        {
            if (ModelState.IsValid)
            {
                ProductCatalogue c = db.ProductCatalogue.Where(m => m.Name == productcatalogue.Name).FirstOrDefault();
                if (c != null)
                {
                    ModelState.AddModelError("Name", "Tên mục đã tồn tại");
                    return View(productcatalogue);
                }
                productcatalogue.AddCreatedTime();
                productcatalogue.URL = UrlUtil.GenerateUrl(productcatalogue.Name);
                productcatalogue.UserID = Int32.Parse(Session["UserID"].ToString());
                db.ProductCatalogue.Add(productcatalogue);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productcatalogue);
        }

        //
        // GET: /Admin/ProductCatalogue/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ProductCatalogue productcatalogue = db.ProductCatalogue.Find(id);
            if (productcatalogue == null)
            {
                return HttpNotFound();
            }
            return View(productcatalogue);
        }

        //
        // POST: /Admin/ProductCatalogue/Edit/5

        [HttpPost]
        public ActionResult Edit(ProductCatalogue productcatalogue)
        {
            if (ModelState.IsValid)
            {
                productcatalogue.UpdateModifiedTime();
                db.Entry(productcatalogue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productcatalogue);
        }

        //
        // GET: /Admin/ProductCatalogue/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ProductCatalogue productcatalogue = db.ProductCatalogue.Find(id);
            if (productcatalogue == null)
            {
                return HttpNotFound();
            }
            return View(productcatalogue);
        }

        //
        // POST: /Admin/ProductCatalogue/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductCatalogue productcatalogue = db.ProductCatalogue.Find(id);
            db.ProductCatalogue.Remove(productcatalogue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}