﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FashionShop.Models;

namespace FashionShop.Areas.Admin.Controllers
{
    public class PostController : AdminAppController
    {
        //private ModelDBContext db = new ModelDBContext();

        //
        // GET: /Admin/Post/

        public ActionResult Index()
        {
            var post = db.Post.Include(p => p.User);
            return View(post.ToList());
        }

        //
        // GET: /Admin/Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Admin/Post/Create

        public ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.User, "UserID", "Username");
            return View();
        }

        //
        // POST: /Admin/Post/Create

        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                post.UserID = Int32.Parse(Session["UserID"].ToString());
                post.AddCreatedTime();
                post.URL = UrlUtil.GenerateUrl(post.Title);

                db.Post.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.User, "UserID", "Username", post.UserID);
            return View(post);
        }

        //
        // GET: /Admin/Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.User, "UserID", "Username", post.UserID);
            return View(post);
        }

        //
        // POST: /Admin/Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                post.UpdateModifiedTime();
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.User, "UserID", "Username", post.UserID);
            return View(post);
        }

        //
        // GET: /Admin/Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Admin/Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Post.Find(id);
            db.Post.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}