﻿//@prepros-prepend qlib.js


$(document).ready(function () {
    (function (h) {
        if (h.indexOf("/ProductCatalogue") != -1)
            return $('#tm-catalogue').addClass('active');

        if (h.indexOf("/Post") != -1)
            return $('#tm-post').addClass('active');

        if (h.indexOf("/Product") != -1)
            return $('#tm-product').addClass('active');

        if (h.indexOf("/Brand") != -1)
            return $('#tm-brand').addClass('active');

        if (h.indexOf("/Location") != -1)
            return $('#lm-location').addClass('active');

        if (h.indexOf("/Image") != -1)
            return $('#lm-image').addClass('active');

        if (h.indexOf("/User") != -1)
            return $('#lm-user').addClass('active');

        if (h.indexOf("/Customer") != -1)
            return $('#lm-customer').addClass('active');

        if (h.indexOf("/User") != -1)
            return $('#lm-user').addClass('active');

        if (h.indexOf("/Group") != -1)
            return $('#lm-group').addClass('active');
    })(location.href);    
});
