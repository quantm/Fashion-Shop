﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FashionShop.Models
{
    public class Option : AppModel
    {
        [Key]
        public int OptionID { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên tùy chọn")]
        [StringLength(255)]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        public string Value { get; set; }
    }
}