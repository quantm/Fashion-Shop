﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class User : AppModel
    {
        [Key]
        public int UserID { get; set; }

        
        [Required(ErrorMessage = "Bạn chưa nhập tên người dùng")]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Tên người dùng phải từ 2 đến 255 kí tự!")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Tên người dùng phải từ 2 đến 255 kí tự!")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Email phải từ 2 đến 255 kí tự!")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Tên hiển thị phải từ 2 đến 255 kí tự!")]
        [Display(Name = "Full name")]
        public string Fullname { get; set; }

        public int? GroupID { get; set; }
        public virtual Group Group { get; set; }

        public bool IsLoggedIn()
        {
            return (HttpContext.Current.Session["Username"] == null) ? false : true;
        }
   
    }
}