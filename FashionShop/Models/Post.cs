﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class Post : AppModel
    {
        [Key]
        public int PostID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tiêu đề bài viết")]
        [StringLength(255, MinimumLength = 5, ErrorMessage = "Tiêu đề phải từ 5 đến 255 kí tự")]
        public string Title { get; set; }

        [StringLength(255, MinimumLength = 5, ErrorMessage = "Mô tả phải từ 5 đến 255 kí tự")]
        public string Description { get; set; }

        public string Content { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        public bool? Status { get; set; }

        public int? UserID { get; set; }
        public virtual User User { get; set; }
    }
}