﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FashionShop.Models
{
    public class Image : AppModel
    {

        [Key]
        public int ImageID { get; set; }

        public int? UserID { get; set; }
        public virtual User User { get; set; }

        public int? ProductID { get; set; }
        public virtual Product Product { get; set; }


        [Url(ErrorMessage = "URL không hợp lệ")]
        public string URL { get; set; }
    }
}