﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class ProductCatalogue : AppModel
    {
        [Key]
        public int ProductCatalogueID { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên mục")]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Tên mục phải từ 5 đến 255 kí tự")]
        public string Name { get; set; }

        [StringLength(255, ErrorMessage = "Mô tả tối đa 255 kí tự")]
        public string Description { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        public int? UserID { get; set; }
        public virtual User User { get; set; }
    }
}