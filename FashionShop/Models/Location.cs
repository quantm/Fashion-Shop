﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class Location : AppModel
    {
        [Key]
        public int LocationID { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên quốc gia")]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Tên quốc gia phải từ 2 đến 255 kí tự!")]
        public string Name { get; set; }

        [StringLength(255, MinimumLength = 2, ErrorMessage = "Mô tả phải từ 2 đến 255 kí tự!")]
        public string Description { get; set; }
    }
}