﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class Product : AppModel
    {
        [Key]
        public int ProductID { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên sản phẩm")]
        [StringLength(255, MinimumLength= 5, ErrorMessage = "Tên sản phẩm phải từ 5 đến 255 kí tự!")]
        public string Name { get; set; }

        [StringLength(255, ErrorMessage = "Mô tả tối đa 255 kí tự")]      
        public string Description { get; set; }

        [StringLength(255)]
        public string URL { get; set; }


        [Required(ErrorMessage = "Bạn chưa nhập giá gốc sản phẩm")]
        public int Price { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập giá bán ra của sản phẩm")]
        public int SalePrice { get; set; }
        
             
        public bool? Status { get; set; }

        /*!Model relations*/
        public int? BrandID { get; set; }
        public virtual Brand Brand { get; set; }

        public int? LocationID { get; set; }
        public virtual Location location { get; set; }

        public int? ProductCatalogueID { get; set; }
        public virtual ProductCatalogue ProductCatalogue { get; set; }

        public int? UserID { get; set; }
        public virtual User User { get; set; }

        public Product()
        {
            this.Status = true;
            this.Description = "";
        }
    }

  
}