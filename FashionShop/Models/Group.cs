﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class Group : AppModel
    {
        [Key]
        public int GroupID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên nhóm")]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Tên nhóm người dùng phải từ 2 đến 255 kí tự!")]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }
    }
}