﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FashionShop.Models
{
    public class Brand : AppModel
    {
        [Key]
        public int BrandID { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên thương hiệu")]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Tên thương hiệu phải từ 2 đến 255 kí tự!")]
        public string Name { get; set; }

        public string ImageURL { get; set; }

        [StringLength(255, ErrorMessage = "Mô tả tối đa 255 kí tự")]
        public string Description { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        public int? UserID { get; set; }
        public virtual User User { get; set; }
    }
}