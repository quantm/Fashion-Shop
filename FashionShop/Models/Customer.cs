﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FashionShop.Models
{
    public class Customer : AppModel
    {
        [Key]
        public int CustomerID { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên")]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Tên phải từ 2 đến 255 kí tự!")]
        public string Fullname { get; set; }

        [StringLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProduct { get; set; }
    }
}