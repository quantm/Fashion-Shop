﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;
namespace FashionShop.Models
{
    public class AppModel
    {
        public DateTime? Modified {get;set;}
        public DateTime? Created { get; set; }

        public AppModel()
        {
            //this.Created = DateTime.Now;
            //this.Modified = DateTime.Now;
        }

        public void UpdateModifiedTime()
        {
            this.Modified = DateTime.Now;
        }

        public void AddCreatedTime()
        {
            this.Created = DateTime.Now;
            this.Modified = DateTime.Now;
        }

        public static string CreateMD5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();
        }

    }

    public class ModelDBContext : DbContext
    {
        public DbSet<Product> Product { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<Brand> Brand { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<Image> Image { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<ProductCatalogue> ProductCatalogue { get; set; }

        public DbSet<Group> Group { get; set; }

        public DbSet<Option> Options { get; set; }

        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerProduct> CustomerProduct { get; set; }

    }
}