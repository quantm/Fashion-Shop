﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace FashionShop.Models
{
    public class CustomerProduct : AppModel
    {
        [Key]
        public int CustomerProductID { get; set; }

        [ForeignKey("Product")]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        //[ForeignKey("Customer")]
        public int CustomerID { get; set; }
        //public virtual Customer Customer { get; set; }
    }
}